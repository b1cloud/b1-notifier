Library written in PHP that allows to communicate with B1 API. [Documentation](https://www.b1.lt/doc/api/)

### Requirements ###

* PHP 7.0

### Installation ###

* Copy B1.php to your project.

### How to use ###

```
#!php

$b1 = new B1(['apiKey' => '', 'privateKey' => '']);
$data = [];
try {
    $response = $b1->request('shop/order/add', $data);
    $data = $response->getContent();
} catch (B1ClientException $e) {
    print_r($e->getMessage());
    print_r($e->getResponse());
    print_r($e->getExtraData());
} catch (B1Exception $e) {
    print_r($e->getMessage());
    print_r($e->getExtraData());
}
```

### Contacts ###

* Question? write to info@b1.lt